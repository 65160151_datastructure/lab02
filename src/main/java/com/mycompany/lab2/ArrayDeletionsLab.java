/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab2;

/**
 *
 * @author User
 */
public class ArrayDeletionsLab {
    public static int[] deleteElementByIndex(int[] arr, int index) {

        int[] newArray = new int[arr.length - 1];

        for (int i = 0; i < index; i++) {
            newArray[i] = arr[i];
        }

        for (int i = index; i < arr.length - 1; i++) {
            newArray[i] = arr[i + 1];
            for (int j = 0; j < i; j++) {
                if (newArray[j] > newArray[j + 1]) {
                    int newNum = newArray[j];
                    newArray[j] = newArray[j + 1];
                    newArray[j + 1] = newNum;
                }
            }
        }

        return newArray;

    }

    public static int[] deleteElementByValue(int[] arr, int value) {
        int[] newArray = new int[arr.length - 1];    
            for(int i=0;i<arr.length;i++){
                if(arr[i]==value){
                    i++;
                    value =-1;
                }
                for (int j = 0; j < arr.length-2; j++) {
                    newArray[j] = arr[i];
                    if (newArray[j] > newArray[j + 1]) {
                        int newNum = newArray[j];
                        newArray[j] = newArray[j + 1];
                        newArray[j + 1] = newNum;
                }
            }
                
            }
        return newArray;

    }

    public static void main(String[] args) {
        int[] array = { 1, 2, 3, 4, 5 };
        int index = 2;
        deleteElementByIndex(array, index);

        for (int i = 0; i < array.length - 1; i++) {
            System.out.println(deleteElementByIndex(array, index)[i]);
        }

        System.out.println("________________");

        int value = 4;
        deleteElementByValue(deleteElementByIndex(array, index), value);

        for (int i = 0; i < deleteElementByIndex(array, index).length - 1; i++) {
            System.out.println(deleteElementByValue(deleteElementByIndex(array, index), value)[i]);
        }
    }
}
