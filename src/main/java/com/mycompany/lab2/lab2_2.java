/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab2;

/**
 *
 * @author User
 */
public class lab2_2 {
    public static int getK(int[] nums, int val) {
        int k = 0;
        System.out.print("Output = {");
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                k++;
            }
            if (nums[i] == val) {
                for (int j = i + 1; j < nums.length; j++) {
                    if (nums[j] != val) {
                        k++;
                        nums[i] = nums[j];
                        nums[j] = val;
                        break;
                    }
                }
            }
            if (nums[i] != val) {
                System.out.print(nums[i]);
            } else {
                System.out.print("_");
            }
            if(i != nums.length-1){
                System.out.print(",");
            }
        }
        System.out.println("}");
        System.out.println("k = " + k);
        return k;
    }

    // public static int[] removeElement(int[] nums, int val) {
    // for (int i = 0; i < nums.length - 1; i++) {
    // if (nums[i] == val) {
    // for (int j = i + 1; j < nums.length; j++) {
    // if (nums[j] != val) {
    // nums[i] = nums[j];
    // nums[j] = val;
    // break;
    // }
    // }
    // }
    // }
    //
    // return nums;
    // }

    // public static void outPut(int k, int[] nums) {
    // System.out.println("k = " + k);
    // System.out.print("Output = {");
    // for (int i = 0; i < nums.length; i++) {
    // if (i <= k - 1) {
    // System.out.print(nums[i] + ",");
    // } else {
    // System.out.print("_,");
    //
    // }
    // }
    // System.out.println("}");
    //
    // }

    // public static void printArray(int[] nums) {
    // for (int i = 0; i < nums.length; i++) {
    // System.out.print(nums[i] + ",");
    // }

    // }

    public static void main(String[] args) {
        int[] nums = { 3, 2, 2, 3 };
        int val = 3;
        getK(nums, val);
        // outPut(getK(nums, val),removeElement(nums, val));

        int[] nums2 = { 0, 1, 2, 2, 3, 0, 4, 2 };
        int val2 = 2;

        // printArray(removeElement(nums2, val2));

        getK(nums2, val2);
        // outPut(getK(nums2, val2),removeElement(nums2, val2));
    }
}
